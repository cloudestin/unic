# unic #

**unic** is a _node.js_ session manager with some advanced features like one session by user, remote disconnect,...

## How does it work ##
**unic** is a server side session manager. Frontend/backend communication is done using HTTP. **unic** permits to:

- register a user (but does not check password, this should be done elsewhere) and create a session to store credentials.
- It permits to autorize only ONE session simultanously for a user. If you try to open a second session from another browser, you will get an error message.
- You can chose to force login and disconnect remotly the user from other location.
- Close the session and abandon all data stored in it.
- You can chose a language for error messages. Currently english(en, default language) and french (fr) are supported but other languages can easily be added. This is done using internationalisation package **qamoos**. As language is stored as a session variable, you should activate session management by using **express-session** middleware.

Almost all the actions return an object in the form of {err: errorCode, msg : message} 

## Example of use ##
**app.js**
```javascript
var express = require('express');
var app = express();
var expressSession = require('express-session');
var uniC = require('unic');

app.use( expressSession({secret:'mySecretKey', cookie: { maxAge: 60000 }, resave:false, saveUninitialized:true}) );

// getLogin gives the current connected login.
function getLogin(req,res){
    res.status(200).json(uniC(req).getLogin() || null).end();
}

// get the value stored in the session  (this could be any objet containing useful information about the user or the session, timestamp,..)
function getStoredValue(req,res){
    res.status(200).json(uniC(req).get()).end();
}

// signin and store a value in the session
function signin(req,res){
    var force = !!parseInt(req.params.force);
    res.status(200).json(uniC(req).signin( req.params.login , force , {} )).end();
}

function signout(req,res){
    uniC(req).signout();
}

// guard is currently similar to getLogin
function guard(req,res){
    res.status(200).json(uniC(req).guard()).end();
}

// change the current language for this session.
// All qamoos in this session will switch to the new language
function lang(req,res){
    uniC(req).lang(req.params.lang);
}

```

The third argument of the ```signin``` method is any object that will be stored in the session. It can be obtained with the .get() method.