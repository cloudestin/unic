﻿function initQamoos(req){
	var qamoos = require("qamoos")(req);
	
	var book = qamoos.define("uniCMsgs");
	book.set('conEnCours', {fr:"Connexion en cours.." , en:"Connection pending.."},-100);
	book.set('otherSessionActive', {fr:"Session ouverte sur un autre poste", en:"Session already opened."},-101);
	book.set('loginOK', {fr:"Connexion réussie", en:"Signin succeeded."},0);
	book.set('logoutOK', {fr:"Déconnexion réussie", en:"Signout succeeded."},0);
	return book;
}


module.exports = function(req){
 var book = initQamoos(req);
 return book;
}