var oneSession = require("./oneSession"),
	varSession,
	sessionID,
	ip,
	qamoos;

module.exports = initialize;

// vérifie si le login 'lg' pourra etre connecté et renvoi un objet err eventuelle
function check(lg,force) {
 guard();
 if (connected()) return qamoos.getErr("conEnCours");
 if (!force && !oneSession.check(lg)) return qamoos.getErr("otherSessionActive");
 return {};
}

// ouvre une session pour l'utilisateur.
// Si "force" alors effectue une déconnexion distente 
function signin(login , force, value ) {
 var allowed = check(login , force);
 
 if (allowed.err) return allowed;
 if (!oneSession.register(login , force , sessionID , ip)) return qamoos.getErr("otherSessionActive");;
 varSession.set( "uniCsession" , {lg:login , val:value||null} );
 return qamoos.getErr("loginOK");
}

function signout() {
 if (connected()) {
	 oneSession.unregister( varSession.get("uniCsession").lg );
	 varSession.abandon();
	}
 return qamoos.getErr("logoutOK");
}

function connected() {
	 guard();
	 return !!varSession.get("uniCsession");
	}

function onConnected(){
  if (connected()) return {yes:function(e){e(); return this;} , no:function(e){return this;} };
  else return {yes:function(e){return this;}, no:function(e){e(); return this;}};
}

function remotelyDisconnected(){
 var session = varSession.get("uniCsession");

 return session && !oneSession.thisSession(session.lg , sessionID);
}

function get() {
	guard();
	var session = varSession.get("uniCsession");
	return (session && session.val) || null;
}
function getLogin() {
	guard();
	var session = varSession.get("uniCsession");
	return (session && session.lg) || null;
}

function guard() {
	 var session = varSession.get("uniCsession"),
	 	 login = (session && session.lg) || null;
	 if (session && session.lg && !oneSession.thisSession(session.lg , sessionID)) {
	 	// remotely disconnected
	 	varSession.abandon();  
	 	login = null;
	 }
	 return login;
	}

function lang( lang ){
	qamoos.lang(lang);
	return this;
}

function initialize(req)
	{
	 varSession = require("varsession")(req);

	 sessionID = req.sessionID;
	 ip = req.ip;
	 qamoos = require("./uniCmsgs")(req);
	 return {
	 	 lang:lang,
		 check:check,
		 signin:signin,
		 signout:signout,
		 connected:connected,
		 onConnected:onConnected,
		 remotelyDisconnected:remotelyDisconnected,
		 get:get,
		 guard:guard,
		 getLogin:getLogin
		};
	}
