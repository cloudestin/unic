//
// 		oneSession
//
// g�re les logins connect�s dans une table
// une seule connexion autoris�e par login
//
// Utilise uine variable serveur (global) pour stocker les sessions
//


// V�rifie si "login" actuellement connect�.
// Si pas connect� retourne true. retourne null sinon.
var varServer = require("varserver")();

var manager = varServer.get("oneSessionManager",{});

exports.check = function(login){
 return !manager[ login ];
}

// Register a session
// Return false if failed
exports.register = function(login, force , id , ip){
 if (!force && manager[ login ]) return false;
 manager[ login ] = {id:id, ip:ip, when:new Date() };
 return true;
}

exports.thisSession = function( login , sessionID ){
 return !!manager[login] && (manager[login].id == sessionID);
}

exports.unregister = function(login){
 delete manager[login];
}
